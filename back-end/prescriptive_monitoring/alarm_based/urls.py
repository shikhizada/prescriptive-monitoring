from django.urls import path
# from . import views
from .views import Method, Recommendation

urlpatterns = [
    path('methods/', Method.get_methods),
    path('recommendations/', Recommendation.recommend)
]