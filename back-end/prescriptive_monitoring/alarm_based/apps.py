from django.apps import AppConfig


class AlarmBasedConfig(AppConfig):
    name = 'alarm_based'
