from django.views.decorators.csrf import csrf_exempt
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from alarm_based.src.constants.method_constants import *
from alarm_based.src.constants.path_constants import *
from alarm_based.src.utils.DatasetManager import DatasetManager

from pm4py.objects.log.importer.xes import factory as xes_import_factory

import os
import csv
import shutil
import requests

class Method:
    @csrf_exempt
    @api_view(['GET'])
    def get_methods(request):
        context = {
            'methods': {
                "optimize_params_and_write_predictions": methods["optimize_params_and_write_predictions"].keys(),
                "optimize_threshold_and_test": methods["optimize_threshold_and_test"].keys(),
            }
        }
        return Response(context, status=status.HTTP_200_OK)


class Recommendation:
    @csrf_exempt
    @api_view(['POST'])
    def recommend(request):
        selected_log_split_id = request.data.get("selected_log_split_id", None)
        selected_methods = request.data.get("selected_methods", None)
        parameters = request.data.get("parameters", None)
        optimize_params_and_write_predictions = selected_methods["optimize_params_and_write_predictions"]
        optimize_threshold_and_test = selected_methods["optimize_threshold_and_test"]
        prefix_length = parameters["prefix_length"]
        weights = parameters["weights"]

        if os.path.exists(settings.MEDIA_ROOT + "/alarm_based"):
            shutil.rmtree(settings.MEDIA_ROOT + "/alarm_based", ignore_errors=True)

        train_url= "http://193.40.11.150/splits/" + str(selected_log_split_id) + "/logs/train"
        test_url = "http://193.40.11.150/splits/" + str(selected_log_split_id) + "/logs/test"
        train_data = requests.get(train_url, allow_redirects=True)
        test_data = requests.get(test_url, allow_redirects=True)

        if os.path.exists(settings.MEDIA_ROOT + "input/log/splits"):
            shutil.rmtree(settings.MEDIA_ROOT + "input/log/splits", ignore_errors=True)
        os.makedirs(os.path.join(settings.MEDIA_ROOT + "input/log/splits/" + str(selected_log_split_id) + "/"))
        train_path = settings.MEDIA_ROOT + "input/log/splits/" + str(selected_log_split_id) + "/train.xes"
        test_path = settings.MEDIA_ROOT + "input/log/splits/" + str(selected_log_split_id) + "/test.xes"

        open(train_path, 'wb').write(train_data.content)
        open(test_path, 'wb').write(test_data.content)

        train_log = xes_import_factory.apply(train_path)
        test_log = xes_import_factory.apply(test_path)

        print('Preparing data...')
        dataset_manager = DatasetManager(train_log, test_log)
        log_name = "log"

        methods["optimize_params_and_write_predictions"][optimize_params_and_write_predictions][0].start(log_name, dataset_manager, prefix_length)
        methods["optimize_params_and_write_predictions"][optimize_params_and_write_predictions][1].start(log_name, dataset_manager, prefix_length)
        for method in methods["optimize_threshold_and_test"][optimize_threshold_and_test]:
            method.start(log_name, weights)

        files = list(filter(lambda file: file.endswith(".csv"), os.listdir(DIR_RESULTS)))
        with open(DIR_RESULTS + files[0], 'r') as f:
            result = list(csv.reader(f, delimiter=';'))
        context = {'result': result}

        return Response(context, status=status.HTTP_200_OK)
