from django.conf import settings

DIR_OPTIMAL_PARAMS = settings.MEDIA_ROOT + "/alarm_based/output/optimal_params/"
DIR_PREDICTIONS = settings.MEDIA_ROOT + "/alarm_based/output/predictions/"
DIR_OPTIMAL_CONFS = settings.MEDIA_ROOT + "/alarm_based/output/optimal_confs/"
DIR_OPTIMAL_FIREDELAY = settings.MEDIA_ROOT + "/alarm_based/output/optimal_firedelay/"
DIR_RESULTS = settings.MEDIA_ROOT + "/alarm_based/output/results/"