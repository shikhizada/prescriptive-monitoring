from alarm_based.src.optimize_params import *
from alarm_based.src.write_predictions import *
from alarm_based.src.optimize_threshold import *
from alarm_based.src.optimize_threshold.hierarchical_thresholding.basic import *
from alarm_based.src.optimize_threshold.hierarchical_thresholding.prefix_length_and_fire_delay import *
from alarm_based.src.test import *

methods = {
    "optimize_params_and_write_predictions": {
        "lgbm": [optimize_params_lgbm, write_lgbm_predictions],
        "rf": [optimize_params_rf, write_rf_predictions]
    },
    "optimize_threshold_and_test": {
        "threshold": [optimize_threshold, test_optimized_threshold],
        "threshold_compensation": [optimize_threshold_compensation, test_optimized_thresholds_compensation],
        "threshold_effectiveness": [optimize_threshold_effectiveness, test_optimized_thresholds_effectiveness],
        "2_prefix_length_dependent_thresholds": [optimize_2_thresholds_prefix, test_2_threholds_prefix],
        "3_prefix_length_dependent_thresholds": [optimize_3_thresholds_prefix, test_3_thresholds_prefix],
        "fire_delay": [optimize_fire_delay, test_fire_delay],
        "fire_delay_prefix_length_dependent_threshold": [optimize_fire_delay_prefix_length_dependent_threshold,
                                                         test_fire_delay_prefix_length_dependent_threshold],
        "hierarchical_thresholding": [optimize_alarm2_threshold_compensation,
                                      optimize_alarm1_threshold_compensation, optimize_alarm2_vs_alarm1_hierachical,
                                      test_multi_1_vs_1_hierachical],
        # "hierarchical_thresholding_prefix_length_and_fire_delay": [
        #     optimize_fire_delay_prefix_length_alarm1_set_fire_delay,
        #     optimize_fire_delay_prefix_length_alarm2_set_fire_delay,
        #     optimize_fire_delay_prefix_length_set_fire_delay_1_vs_2,
        #     test_hierarchical_fire_delay_prefix_length_1_vs_2]
    }
}
