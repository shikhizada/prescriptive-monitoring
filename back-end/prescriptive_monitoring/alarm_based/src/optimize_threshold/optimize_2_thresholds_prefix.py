from alarm_based.src.constants import *

import pandas as pd
import numpy as np

import os
import pickle

from hyperopt import Trials, STATUS_OK, tpe, fmin, hp
import hyperopt


def start(dataset_name, weights):
    def calculate_cost(x, costs):
        return costs[int(x['prediction']), int(x['actual'])](x)

    def evaluate_model_cost(args):
        conf_thresholds = args['conf_threshold']
        c_action = args['c_action']
        c_miss = args['c_miss']
        c_com = args['c_com']
        prefix_threshold = args['prefix_threshold']

        if early_type == "linear":
            costs = np.matrix([[lambda x: 0,
                                lambda x: c_miss],
                               [lambda x: c_action * (x['prefix_nr'] - 1) / x['case_length'] + c_com,
                                lambda x: c_action * (x['prefix_nr'] - 1) / x['case_length'] + (x['prefix_nr']) / x[
                                    'case_length'] * c_miss
                                ]])
        else:
            costs = np.matrix([[lambda x: 0,
                                lambda x: c_miss],
                               [lambda x: c_action + c_com,
                                lambda x: c_action + (x['prefix_nr'] - 1) / x['case_length'] * c_miss
                                ]])

        # trigger alarms according to conf_threshold
        dt_final = pd.DataFrame()
        unprocessed_case_ids = set(dt_preds.case_id.unique())
        for nr_events in range(1, dt_preds.prefix_nr.max() + 1):
            if nr_events < prefix_threshold:
                conf_threshold = conf_thresholds[0]
            else:
                conf_threshold = conf_thresholds[1]
            tmp = dt_preds[(dt_preds.case_id.isin(unprocessed_case_ids)) & (dt_preds.prefix_nr == nr_events)]
            tmp = tmp[tmp.predicted_proba >= conf_threshold]
            tmp["prediction"] = 1
            dt_final = pd.concat([dt_final, tmp], axis=0)
            unprocessed_case_ids = unprocessed_case_ids.difference(tmp.case_id)
        tmp = dt_preds[(dt_preds.case_id.isin(unprocessed_case_ids)) & (dt_preds.prefix_nr == 1)]
        tmp["prediction"] = 0
        dt_final = pd.concat([dt_final, tmp], axis=0)

        case_lengths = dt_preds.groupby("case_id").prefix_nr.max().reset_index()
        case_lengths.columns = ["case_id", "case_length"]
        dt_final = dt_final.merge(case_lengths)

        cost = dt_final.apply(calculate_cost, costs=costs, axis=1).sum()

        return {'loss': cost, 'status': STATUS_OK, 'model': dt_final}

    def run_experiment(c_miss_weight, c_action_weight, c_com_weight, early_type):
        c_miss = c_miss_weight / (c_miss_weight + c_action_weight + c_com_weight)
        c_action = c_action_weight / (c_miss_weight + c_action_weight + c_com_weight)
        c_com = c_com_weight / (c_miss_weight + c_action_weight + c_com_weight)

        conf_thresholds = []
        for i in range(2):
            string_conf_threshold = "conf_threshold" + str(i)
            conf_thresholds.append(hp.uniform(string_conf_threshold, 0, 1))

        space = {'conf_threshold': conf_thresholds,
                 'c_action': c_action,
                 'c_miss': c_miss,
                 'prefix_threshold': hp.choice("prefix_threshold", range(1, dt_preds.prefix_nr.max() + 1)),
                 'c_com': c_com}
        trials = Trials()
        best = fmin(evaluate_model_cost, space, algo=tpe.suggest, max_evals=150, trials=trials)

        best_params = hyperopt.space_eval(space, best)

        outfile = os.path.join(DIR_OPTIMAL_CONFS, "optimal_confs_%s_%s_%s_%s_%s_%s.pickle" % (
            dataset_name, c_miss_weight, c_action_weight, c_postpone_weight, c_com_weight, early_type))
        # write to file
        with open(outfile, "wb") as fout:
            print(outfile)
            print(repr(best_params))
            pickle.dump(best_params, fout)

    print('Preparing data...')

    # create output directory
    if not os.path.exists(os.path.join(DIR_OPTIMAL_CONFS)):
        os.makedirs(os.path.join(DIR_OPTIMAL_CONFS))

    # prepare the dataset
    dt_preds = pd.read_csv(os.path.join(DIR_PREDICTIONS, "preds_val_%s.csv" % dataset_name), sep=";")

    print('Optimizing parameters...')
    c_postpone_weight = weights["c_postpone_weight"]
    for early_type in ["const", "linear"]:
        run_experiment(weights["c_miss_weight"], weights["c_action_weight"], weights["c_com_weight"], early_type)
