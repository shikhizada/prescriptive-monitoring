import pandas as pd

derived_trace_attrs = ["label"]
derived_event_attrs = ["event_nr", "month", "weekday", "hour",
                       "timesincemidnight", "timesincelastevent", "timesincecasestart"]


def generate_core_attrs(log):
    core_trace_attrs = set()
    core_event_attrs = set()

    for trace in log:
        core_trace_attrs.update(trace.attributes.keys())
        for event in trace:
            core_event_attrs.update(event.keys())

    core_trace_attrs.remove("concept:name")
    core_trace_attrs.add("Case ID")

    core_event_attrs.difference_update({"concept:name", "time:timestamp"})
    core_event_attrs.update({"Activity", "Complete Timestamp"})

    return list(core_trace_attrs), list(core_event_attrs)


def generate_trace_values(trace, time_threshold):
    trace_values = {}

    for trace_attr in trace.attributes.keys():
        if trace_attr == "concept:name":
            trace_values["Case ID"] = trace.attributes[trace_attr]
        else:
            trace_values[trace_attr] = trace.attributes[trace_attr]

    if (trace[len(trace) - 1]["time:timestamp"] - trace[0]["time:timestamp"]).seconds < time_threshold:
        trace_values["label"] = "fast"
    else:
        trace_values["label"] = "slow"
    return trace_values


def generate_event_values(log, ind_curr_trace, ind_curr_event):
    event_values = {}

    curr_event = log[ind_curr_trace][ind_curr_event]
    complete_timestamp = curr_event["time:timestamp"]

    for event_attr in curr_event.keys():
        if event_attr == "concept:name":
            event_values["Activity"] = curr_event[event_attr]
        elif event_attr == "time:timestamp":
            event_values["Complete Timestamp"] = curr_event[event_attr]
        else:
            event_values[event_attr] = curr_event[event_attr]

    event_values["event_nr"] = ind_curr_event
    event_values["month"] = complete_timestamp.month
    event_values["weekday"] = complete_timestamp.weekday()
    event_values["hour"] = complete_timestamp.hour

    event_values["timesincemidnight"] = complete_timestamp.hour * 60 + complete_timestamp.minute

    if ind_curr_event == 0:
        event_values["timesincelastevent"] = 0
    else:
        event_values["timesincelastevent"] = (complete_timestamp -
                                              log[ind_curr_trace][ind_curr_event - 1]["time:timestamp"]).seconds / 60

    event_values["timesincecasestart"] = (complete_timestamp - log[ind_curr_trace][0]["time:timestamp"]).seconds / 60

    return event_values


def calc_time_threshold(log):
    time_threshold = 0
    for trace in log:
        time_threshold += (trace[len(trace) - 1]["time:timestamp"] - trace[0]["time:timestamp"]).seconds
    time_threshold /= len(log)
    return time_threshold


def generate_df_from_xes(log):
    rows = []
    time_threshold = calc_time_threshold(log)

    for ind_trace, trace in enumerate(log):
        print("ind_trace: ", ind_trace)
        trace_values = generate_trace_values(trace, time_threshold)
        for ind_event, event in enumerate(trace):
            event_values = generate_event_values(log, ind_trace, ind_event)
            rows.append({**event_values, **trace_values})
    df = pd.DataFrame.from_dict(rows, orient='columns')
    # df.to_csv(OUT_CSV_LOG_PATH)
    return df


def generate_df_and_features(log):
    dynamic_cat_cols = []
    static_cat_cols = []
    dynamic_num_cols = []
    static_num_cols = []

    core_trace_attrs, core_event_attrs = generate_core_attrs(log)
    data = generate_df_from_xes(log)

    for trace_attr in core_trace_attrs + derived_trace_attrs:
        if trace_attr not in ["label", "Case ID"]:
            if pd.api.types.is_numeric_dtype(data[trace_attr]):
                static_num_cols.append(trace_attr)
            else:
                static_cat_cols.append(trace_attr)

    for event_attr in core_event_attrs + derived_event_attrs:
        if event_attr not in ["Complete Timestamp"]:
            if pd.api.types.is_numeric_dtype(data[event_attr]):
                dynamic_num_cols.append(event_attr)
            else:
                dynamic_cat_cols.append(event_attr)

    dtypes = {}
    for column in data.columns:
        if column in dynamic_num_cols + static_num_cols:
            dtypes[column] = "float"
        else:
            dtypes[column] = "object"
    data = data.astype(dtypes)
    # data["Complete Timestamp"] = pd.to_datetime(data["Complete Timestamp"])

    result = {"static_cat_cols": static_cat_cols,
              "static_num_cols": static_num_cols,
              "dynamic_cat_cols": dynamic_cat_cols,
              "dynamic_num_cols": dynamic_num_cols,
              "data": data}

    return result
