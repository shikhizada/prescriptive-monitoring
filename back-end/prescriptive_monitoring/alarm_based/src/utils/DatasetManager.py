import pandas as pd
import numpy as np

from alarm_based.src.utils import converter as cnv

from sklearn.model_selection import StratifiedKFold

class DatasetManager:

    def __init__(self, train_log, test_log):
        df_and_features_train_log = cnv.generate_df_and_features(train_log)
        df_and_features_test_log = cnv.generate_df_and_features(test_log)

        self.train = df_and_features_train_log["data"]
        self.test = df_and_features_test_log["data"]
        self.data = pd.concat([self.train, self.test])
        self.case_id_col = "Case ID"
        self.activity_col = "Activity"
        self.timestamp_col = "Complete Timestamp"
        self.label_col = "label"
        self.pos_label = "fast"

        self.dynamic_cat_cols = df_and_features_train_log["dynamic_cat_cols"]
        self.static_cat_cols = df_and_features_train_log["static_cat_cols"]
        self.dynamic_num_cols = df_and_features_train_log["dynamic_num_cols"]
        self.static_num_cols = df_and_features_train_log["static_num_cols"]

        self.sorting_cols = [self.timestamp_col, self.activity_col]

    def split_val(self, data, val_ratio, split="random", seed=22):
        # split into train and test using temporal split
        grouped = data.groupby(self.case_id_col)
        start_timestamps = grouped[self.timestamp_col].min().reset_index()
        if split == "temporal":
            start_timestamps = start_timestamps.sort_values(self.timestamp_col, ascending=True, kind="mergesort")
        elif split == "random":
            np.random.seed(seed)
            start_timestamps = start_timestamps.reindex(np.random.permutation(start_timestamps.index))
        val_ids = list(start_timestamps[self.case_id_col])[-int(val_ratio * len(start_timestamps)):]
        val = data[data[self.case_id_col].isin(val_ids)].sort_values(self.sorting_cols, ascending=True,
                                                                     kind="mergesort")
        train = data[~data[self.case_id_col].isin(val_ids)].sort_values(self.sorting_cols, ascending=True,
                                                                        kind="mergesort")
        return (train, val)

    def generate_prefix_data(self, data, min_length, max_length):
        # generate prefix data (each possible prefix becomes a trace)
        data['case_length'] = data.groupby(self.case_id_col)[self.activity_col].transform(len)

        dt_prefixes = data[data['case_length'] >= min_length].groupby(self.case_id_col).head(min_length)
        dt_prefixes["prefix_nr"] = 1
        dt_prefixes["orig_case_id"] = dt_prefixes[self.case_id_col]
        for nr_events in range(min_length + 1, max_length + 1):
            tmp = data[data['case_length'] >= nr_events].groupby(self.case_id_col).head(nr_events)
            tmp["orig_case_id"] = tmp[self.case_id_col]
            tmp[self.case_id_col] = tmp[self.case_id_col].apply(lambda x: "%s_%s" % (x, nr_events))
            tmp["prefix_nr"] = nr_events
            dt_prefixes = pd.concat([dt_prefixes, tmp], axis=0)

        dt_prefixes['case_length'] = dt_prefixes['case_length'].apply(lambda x: min(max_length, x))

        return dt_prefixes

    def get_pos_case_length_quantile(self, data, quantile=0.90):
        return int(
            np.ceil(data[data[self.label_col] == self.pos_label].groupby(self.case_id_col).size().quantile(quantile)))

    def get_label(self, data):
        return data.groupby(self.case_id_col).first()[self.label_col]

    def get_label_numeric(self, data):
        y = self.get_label(data)  # one row per case
        return [1 if label == self.pos_label else 0 for label in y]

    def get_idx_split_generator(self, dt_for_splitting, n_splits=5, shuffle=True, random_state=22):
        skf = StratifiedKFold(n_splits=n_splits, shuffle=shuffle, random_state=random_state)

        for train_index, test_index in skf.split(dt_for_splitting, dt_for_splitting[self.label_col]):
            current_train_names = dt_for_splitting[self.case_id_col][train_index]
            current_test_names = dt_for_splitting[self.case_id_col][test_index]
            yield (current_train_names, current_test_names)
