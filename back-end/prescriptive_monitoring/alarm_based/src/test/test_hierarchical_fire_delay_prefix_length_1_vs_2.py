from alarm_based.src.constants import *

import pandas as pd
import numpy as np

from sklearn.metrics import precision_recall_fscore_support, confusion_matrix

import os
import csv
import pickle


def start(dataset_name, weights):
    def calculate_cost(x, costs):
        return costs[int(x['prediction']), int(x['actual'])](x)

    def calculate_cost_baseline(x, costs):
        return costs[0, int(x['actual'])](x)

    method = "1_vs_1_hierachical_fire_delay"

    # create results directory
    if not os.path.exists(os.path.join(DIR_RESULTS)):
        os.makedirs(os.path.join(DIR_RESULTS))

    # load predictions
    dt_preds = pd.read_csv(os.path.join(DIR_PREDICTIONS, "preds_%s.csv" % dataset_name), sep=";")

    # write results to file
    out_filename = os.path.join(DIR_RESULTS, "results_%s_%s.csv" % (dataset_name, method))

    with open(out_filename, 'w') as fout:
        writer = csv.writer(fout, delimiter=';', quotechar='', quoting=csv.QUOTE_NONE)
        writer.writerow(
            ["dataset", "method", "metric", "value", "c_miss", "c_action", "c_postpone", "c_com", "early_type",
             "threshold"])

        c_miss_weight = weights["c_miss_weight"]
        c_action_weight = weights["c_action_weight"]
        c_com_weight = weights["c_com_weight"]
        c_postpone_weight = weights["c_postpone_weight"]
        for early_type in ["const", "linear"]:
            c_miss = c_miss_weight / (c_miss_weight + c_action_weight + c_com_weight)
            c_action = c_action_weight / (c_miss_weight + c_action_weight + c_com_weight)
            c_com = c_com_weight / (c_miss_weight + c_action_weight + c_com_weight)

            c_action2 = c_action * 1.2
            c_com2 = c_com * 0.5

            if early_type == "linear":
                costs = np.matrix([[lambda x: 0,
                                    lambda x: c_miss],
                                   [lambda x: c_action * (x['prefix_nr'] - 1) / x['case_length'] + c_com,
                                    lambda x: c_action * (x['prefix_nr'] - 1) / x['case_length'] + (
                                        x['prefix_nr']) / x[
                                                  'case_length'] * c_miss
                                    ],
                                   [lambda x: c_action2 * (x['prefix_nr'] - 1) / x['case_length'] + c_com2,
                                    lambda x: c_action2 * (x['prefix_nr'] - 1) / x['case_length'] + (
                                        x['prefix_nr']) / x[
                                                  'case_length'] * c_miss
                                    ]])
            else:
                costs = np.matrix([[lambda x: 0,
                                    lambda x: c_miss],
                                   [lambda x: c_action + c_com,
                                    lambda x: c_action + (x['prefix_nr'] - 1) / x['case_length'] * c_miss
                                    ],
                                   [lambda x: c_action2 + c_com2,
                                    lambda x: c_action2 + (x['prefix_nr'] - 1) / x['case_length'] * c_miss
                                    ]])

            # load the optimal confidence threshold
            conf_file_0_vs_1 = os.path.join(DIR_OPTIMAL_CONFS, "0_vs_1/",
                                            "optimal_confs_%s_%s_%s_%s_%s_%s.pickle" % (
                                                dataset_name, c_miss_weight, c_action_weight, c_postpone_weight,
                                                c_com_weight, early_type))

            # load the optimal confidence threshold
            conf_file_0_vs_2 = os.path.join(DIR_OPTIMAL_CONFS, "0_vs_2/",
                                            "optimal_confs_%s_%s_%s_%s_%s_%s.pickle" % (
                                                dataset_name, c_miss_weight, c_action_weight, c_postpone_weight,
                                                c_com_weight, early_type))
            # load the optimal confidence threshold
            conf_file_1_vs_2 = os.path.join(DIR_OPTIMAL_CONFS, "0_vs_2/",
                                            "optimal_confs_%s_%s_%s_%s_%s_%s.pickle" % (
                                                dataset_name, c_miss_weight, c_action_weight, c_postpone_weight,
                                                c_com_weight, early_type))

            with open(conf_file_0_vs_1, "rb") as fin:
                conf_param = pickle.load(fin)
                conf_thresholds_0_vs_1 = conf_param['conf_threshold']
                prefix_threshold_0_vs_1 = conf_param['prefix_threshold']
            with open(conf_file_0_vs_2, "rb") as fin:
                conf_param = pickle.load(fin)
                conf_thresholds_0_vs_2 = conf_param['conf_threshold']
                prefix_threshold_0_vs_2 = conf_param['prefix_threshold']
            with open(conf_file_1_vs_2, "rb") as fin:
                conf_param = pickle.load(fin)
                conf_thresholds_1_vs_2 = conf_param['conf_threshold']
                prefix_threshold_1_vs_2 = conf_param['prefix_threshold']

            fire_delay_file = os.path.join(DIR_OPTIMAL_FIREDELAY,
                                           "optimal_firedelay_%s_%s_%s_%s_%s_%s.pickle" % (
                                               dataset_name, c_miss_weight, c_action_weight, c_postpone_weight,
                                               c_com_weight, early_type))
            with open(fire_delay_file, "rb") as fin:
                best_fire_delay_params = pickle.load(fin)
                myopic_param = best_fire_delay_params['myopic_param']

            # do something
            # trigger alarms according to conf_threshold
            dt_final = pd.DataFrame()
            unprocessed_case_ids = set(dt_preds.case_id.unique())
            case_counter = pd.DataFrame()
            case_counter["case_id"] = dt_preds.case_id.unique()
            case_counter["counter"] = 0
            for nr_events in range(1, dt_preds.prefix_nr.max() + 1):
                if nr_events < prefix_threshold_0_vs_1:
                    conf_threshold_0_vs_1 = conf_thresholds_0_vs_1[0]
                else:
                    conf_threshold_0_vs_1 = conf_thresholds_0_vs_1[1]

                if nr_events < prefix_threshold_0_vs_2:
                    conf_threshold_0_vs_2 = conf_thresholds_0_vs_2[0]
                else:
                    conf_threshold_0_vs_2 = conf_thresholds_0_vs_2[1]

                if nr_events < prefix_threshold_1_vs_2:
                    conf_threshold_1_vs_2 = conf_thresholds_1_vs_2[0]
                else:
                    conf_threshold_1_vs_2 = conf_thresholds_1_vs_2[1]

                min_threshold = min(conf_threshold_0_vs_1, conf_threshold_0_vs_2)
                tmp = dt_preds[
                    (dt_preds.case_id.isin(unprocessed_case_ids)) & (dt_preds.prefix_nr == nr_events)]
                tmp = tmp[(tmp.predicted_proba >= min_threshold)]
                case_counter.loc[case_counter.case_id.isin(tmp.case_id), ['counter']] = case_counter[
                                                                                            "counter"] + 1
                tmp_case_counter = case_counter[case_counter.counter > myopic_param]

                tmp = dt_preds[
                    (dt_preds.case_id.isin(unprocessed_case_ids)) & (dt_preds.prefix_nr == nr_events)]
                tmp = tmp[
                    (tmp.predicted_proba >= conf_threshold_0_vs_1) & (
                            tmp.predicted_proba >= conf_threshold_1_vs_2)]
                tmp["prediction"] = 1
                tmp = tmp[tmp.case_id.isin(tmp_case_counter.case_id)]
                dt_final = pd.concat([dt_final, tmp], axis=0)
                unprocessed_case_ids = unprocessed_case_ids.difference(tmp.case_id)

                tmp = dt_preds[
                    (dt_preds.case_id.isin(unprocessed_case_ids)) & (dt_preds.prefix_nr == nr_events)]
                tmp = tmp[
                    (tmp.predicted_proba >= conf_threshold_0_vs_2) & (
                            tmp.predicted_proba <= conf_threshold_1_vs_2)]
                tmp["prediction"] = 2
                tmp = tmp[tmp.case_id.isin(tmp_case_counter.case_id)]
                dt_final = pd.concat([dt_final, tmp], axis=0)
                unprocessed_case_ids = unprocessed_case_ids.difference(tmp.case_id)
            tmp = dt_preds[(dt_preds.case_id.isin(unprocessed_case_ids)) & (dt_preds.prefix_nr == 1)]
            tmp["prediction"] = 0
            dt_final = pd.concat([dt_final, tmp], axis=0)

            case_lengths = dt_preds.groupby("case_id").prefix_nr.max().reset_index()
            case_lengths.columns = ["case_id", "case_length"]
            dt_final = dt_final.merge(case_lengths)

            cost = dt_final.apply(calculate_cost, costs=costs, axis=1).sum()
            writer.writerow(
                [dataset_name, method, "cost", cost, c_miss_weight, c_action_weight, c_postpone_weight,
                 c_com_weight, early_type, conf_threshold_1_vs_2])
            writer.writerow(
                [dataset_name, method, "cost_avg", cost / len(dt_final), c_miss_weight, c_action_weight,
                 c_postpone_weight, c_com_weight, early_type, conf_threshold_1_vs_2])

            cost_baseline = dt_final.apply(calculate_cost_baseline, costs=costs, axis=1).sum()
            writer.writerow(
                [dataset_name, method, "cost_baseline", cost_baseline, c_miss_weight, c_action_weight,
                 c_postpone_weight, c_com_weight, early_type, conf_threshold_1_vs_2])
            writer.writerow(
                [dataset_name, method, "cost_avg_baseline", cost_baseline / len(dt_final), c_miss_weight,
                 c_action_weight, c_postpone_weight, c_com_weight, early_type, conf_threshold_1_vs_2])

            dt_final.prediction = dt_final.prediction.replace(2, 1)
            # print(dt_final.prediction)

            # calculate precision, recall etc.
            prec, rec, fscore, _ = precision_recall_fscore_support(dt_final.actual, dt_final.prediction,
                                                                   pos_label=1, average="binary")
            tn, fp, fn, tp = confusion_matrix(dt_final.actual, dt_final.prediction).ravel()

            # calculate earliness based on the "true alarms" only
            tmp = dt_final[(dt_final.prediction == 1) & (dt_final.actual == 1)]
            earliness = (1 - ((tmp.prefix_nr - 1) / tmp.case_length))
            tmp = dt_final[(dt_final.prediction == 1)]
            earliness_alarms = (1 - ((tmp.prefix_nr - 1) / tmp.case_length))

            writer.writerow(
                [dataset_name, method, "fire_delay", myopic_param, c_miss_weight, c_action_weight,
                 c_postpone_weight, c_com_weight,
                 early_type, conf_threshold_1_vs_2])
            writer.writerow(
                [dataset_name, method, "prec", prec, c_miss_weight, c_action_weight, c_postpone_weight,
                 c_com_weight, early_type, conf_threshold_1_vs_2])
            writer.writerow(
                [dataset_name, method, "rec", rec, c_miss_weight, c_action_weight, c_postpone_weight,
                 c_com_weight,
                 early_type, conf_threshold_1_vs_2])
            writer.writerow(
                [dataset_name, method, "fscore", fscore, c_miss_weight, c_action_weight, c_postpone_weight,
                 c_com_weight, early_type, conf_threshold_1_vs_2])
            writer.writerow(
                [dataset_name, method, "tn", tn, c_miss_weight, c_action_weight, c_postpone_weight,
                 c_com_weight,
                 early_type, conf_threshold_1_vs_2])
            writer.writerow(
                [dataset_name, method, "fp", fp, c_miss_weight, c_action_weight, c_postpone_weight,
                 c_com_weight,
                 early_type, conf_threshold_1_vs_2])
            writer.writerow(
                [dataset_name, method, "fn", fn, c_miss_weight, c_action_weight, c_postpone_weight,
                 c_com_weight,
                 early_type, conf_threshold_1_vs_2])
            writer.writerow(
                [dataset_name, method, "tp", tp, c_miss_weight, c_action_weight, c_postpone_weight,
                 c_com_weight,
                 early_type, conf_threshold_1_vs_2])
            writer.writerow(
                [dataset_name, method, "earliness_mean", earliness.mean(), c_miss_weight, c_action_weight,
                 c_postpone_weight, c_com_weight, early_type, conf_threshold_1_vs_2])
            writer.writerow(
                [dataset_name, method, "earliness_std", earliness.std(), c_miss_weight, c_action_weight,
                 c_postpone_weight, c_com_weight, early_type, conf_threshold_1_vs_2])
            writer.writerow(
                [dataset_name, method, "earliness_alarms_mean", earliness_alarms.mean(), c_miss_weight,
                 c_action_weight, c_postpone_weight, c_com_weight, early_type, conf_threshold_1_vs_2])
            writer.writerow(
                [dataset_name, method, "earliness_alarms_std", earliness_alarms.std(), c_miss_weight,
                 c_action_weight, c_postpone_weight, c_com_weight, early_type, conf_threshold_1_vs_2])
