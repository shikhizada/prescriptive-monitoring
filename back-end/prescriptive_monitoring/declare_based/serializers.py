from rest_framework import serializers


class RecommendationSerializer(serializers.Serializer):
    impurity = serializers.FloatField()
    decision = serializers.CharField()
