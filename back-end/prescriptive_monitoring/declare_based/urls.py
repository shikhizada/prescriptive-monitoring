from django.urls import path
# from . import views
from .views import Log, Declare, DeclareTemplates, Recommendation

urlpatterns = [
    path('logs/', Log.logs),
    path('logs/<str:log>', Log.delete_log),
    path('declares/', Declare.declares),
    path('declares/<str:declare>/', Declare.delete_declare),
    path('declare-templates/', DeclareTemplates.generate),
    path('recommendations/', Recommendation.recommend),
]
