from django.conf import settings
from django.core.files.storage import FileSystemStorage
from django.views.decorators.csrf import csrf_exempt
from declare_based.src.parsers import *
from declare_based.src.machine_learning import *
from .serializers import *
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from pm4py.objects.log.importer.xes import factory as xes_import_factory

import os
import json
import shutil
import requests


class Log:

    @csrf_exempt
    @api_view(['GET', 'POST'])
    def logs(request):
        if request.method == 'POST':
            return Log.upload_log(request)
        else:
            return Log.get_logs(request)

    def upload_log(request):
        if "log" in request.FILES.keys():
            log = request.FILES['log']
            fs = FileSystemStorage()
            fs.save("input/log/" + log.name, log)
            return Response({}, status=status.HTTP_200_OK)
        return Response({}, status=status.HTTP_409_CONFLICT)

    def get_logs(request):
        files = os.listdir(settings.MEDIA_ROOT + "input/log/")
        logs = list(filter(lambda file: file.endswith(".xes"), files))
        context = {'logs': logs}
        return Response(context, status=status.HTTP_200_OK)

    @csrf_exempt
    @api_view(['DELETE'])
    def delete_log(request, log):
        log_path = os.path.join(settings.MEDIA_ROOT + "input/log/", log)
        if os.path.isfile(log_path):
            os.remove(log_path)
            return Response({}, status=status.HTTP_200_OK)
        return Response({}, status=status.HTTP_404_NOT_FOUND)


class Declare:

    @csrf_exempt
    @api_view(['GET', 'POST'])
    def declares(request):
        if request.method == 'POST':
            return Declare.upload_declare(request)
        else:
            return Declare.get_declares(request)

    def upload_declare(request):
        if "declare" in request.FILES.keys():
            declare = request.FILES['declare']
            fs = FileSystemStorage()
            fs.save("input/declare/" + declare.name, declare)
            return Response({}, status=status.HTTP_200_OK)
        return Response({}, status=status.HTTP_409_CONFLICT)

    def get_declares(request):
        files = os.listdir(settings.MEDIA_ROOT + "input/declare/")
        declares = list(filter(lambda file: file.endswith(".decl"), files))
        context = {'declares': declares}
        return Response(context, status=status.HTTP_200_OK)

    @csrf_exempt
    @api_view(['DELETE'])
    def delete_declare(request, declare):
        declare_path = os.path.join(settings.MEDIA_ROOT + "input/declare/", declare)
        if os.path.isfile(declare_path):
            os.remove(declare_path)
            return Response({}, status=status.HTTP_200_OK)
        return Response({}, status=status.HTTP_404_NOT_FOUND)


class DeclareTemplates:
    @csrf_exempt
    @api_view(['POST'])
    def generate(request):
        log = request.data.get("log", None)
        declare = request.data.get("declare", None)
        done = json.loads(request.data.get('done', None))

        log_path = settings.MEDIA_ROOT + "input/log/" + log
        declare_path = settings.MEDIA_ROOT + "input/declare/" + declare

        log = xes_import_factory.apply(log_path)
        declare = parse_decl(declare_path)

        activities = declare["activities"]
        result = {}
        for key, rules in declare["rules"].items():
            if key == INIT:
                result[key] = DT_LOG_METHODS[key](log, done, activities["A"], rules["activation_rules"]).__dict__
            elif key in [EXISTENCE, ABSENCE, EXACTLY]:
                result[key] = DT_LOG_METHODS[key](log, done, activities["A"], rules["activation_rules"],
                                                  rules["n"]).__dict__
            elif key in [CHOICE, EXCLUSIVE_CHOICE]:
                result[key] = DT_LOG_METHODS[key](log, done, activities["A"], activities["B"],
                                                  rules["activation_rules"]).__dict__
            else:
                result[key] = DT_LOG_METHODS[key](log, done, activities["A"], activities["B"],
                                                  rules["activation_rules"],
                                                  rules["correlation_rules"]).__dict__
        context = {"result": result}
        return Response(context, status=status.HTTP_200_OK)

class Recommendation:
    @csrf_exempt
    @api_view(['POST'])
    def recommend(request):
        selected_log_split_id = request.data.get("selected_log_split_id", None)
        thresholds = request.data.get("thresholds", None)
        rules = request.data.get("rules", None)

        average_time = thresholds["average_time"]
        frequency_threshold = thresholds["frequency_threshold"]
        if not average_time:
            time_threshold = float(thresholds["time_threshold"])
        else:
            time_threshold = None

        rules["activation"] = generate_rules(rules["activation"])
        rules["correlation"] = generate_rules(rules["correlation"])

        train_url= "http://193.40.11.150/splits/" + str(selected_log_split_id) + "/logs/train"
        test_url = "http://193.40.11.150/splits/" + str(selected_log_split_id) + "/logs/test"
        train_data = requests.get(train_url, allow_redirects=True)
        test_data = requests.get(test_url, allow_redirects=True)

        if os.path.exists(settings.MEDIA_ROOT + "input/log/splits"):
            shutil.rmtree(settings.MEDIA_ROOT + "input/log/splits", ignore_errors=True)
        os.makedirs(os.path.join(settings.MEDIA_ROOT + "input/log/splits/" + str(selected_log_split_id) + "/"))
        train_path = settings.MEDIA_ROOT + "input/log/splits/" + str(selected_log_split_id) + "/train.xes"
        test_path = settings.MEDIA_ROOT + "input/log/splits/" + str(selected_log_split_id) + "/test.xes"

        open(train_path, 'wb').write(train_data.content)
        open(test_path, 'wb').write(test_data.content)

        train_log = xes_import_factory.apply(train_path)
        test_log = xes_import_factory.apply(test_path)

        recommendations = generate_recommendations(test_log=test_log, train_log=train_log,
                                                   time_threshold=time_threshold,
                                                   frequency_threshold=float(frequency_threshold),
                                                   activation_rules=rules["activation"],
                                                   correlation_rules=rules["correlation"])
        for key in recommendations.keys():
            serializer = RecommendationSerializer(recommendations[key], many=True)
            recommendations[key] = serializer.data
        context = {"recommendations": recommendations}
        return Response(context, status=status.HTTP_200_OK)
