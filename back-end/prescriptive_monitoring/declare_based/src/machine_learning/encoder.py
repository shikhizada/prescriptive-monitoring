from declare_based.src.enums import TraceSpeed
from declare_based.src.constants import *
from declare_based.src.declare_templates.trace import *
from itertools import combinations


def calc_avg_time_threshold(log):
    total_seconds = 0
    for trace in log:
        total_seconds += (trace[len(trace) - 1]["time:timestamp"] - trace[0]["time:timestamp"]).total_seconds()
    avg_time_threshold = total_seconds / len(log)
    return avg_time_threshold


def check_trace_speed(trace, time_threshold):
    time_diff = (
            trace[len(trace) - 1]["time:timestamp"] - trace[0]["time:timestamp"]
    ).total_seconds()
    if time_diff < time_threshold:
        result = TraceSpeed.FAST
    else:
        result = TraceSpeed.SLOW
    return result


def generate_labels(log, time_threshold):
    result = []

    if time_threshold is None:
        time_threshold = calc_avg_time_threshold(log)

    for trace in log:
        result.append(check_trace_speed(trace, time_threshold).value)
    return result


def get_num_traces_by_two_events(log, a, b):
    num_traces_satisfied = 0
    for trace in log:
        a_exists = False
        b_exists = False
        for event in trace:
            if not a_exists and event["concept:name"] == a:
                a_exists = True
            elif not b_exists and event["concept:name"] == b:
                b_exists = True
            if a_exists and b_exists:
                break
        if a_exists and b_exists:
            num_traces_satisfied += 1
    return num_traces_satisfied


# a-priori algorithm
# Description:
# pairs of events and their support (the % of traces where the pair of events occurs)
def a_priori(log):
    num_traces = len(log)
    distinct_events = set()
    result = {}
    for trace in log:
        for event in trace:
            distinct_events.add(event["concept:name"])
    pairs = list(combinations(distinct_events, 2))
    for pair in pairs:
        result[pair] = get_num_traces_by_two_events(log, pair[0], pair[1]) / num_traces
    return result


def encode_trace(log, frequency_threshold, activation_rules, correlation_rules):
    frequent_pairs = [*{k: v for (k, v) in a_priori(log).items() if v > frequency_threshold}]
    pairs = []
    for pair in frequent_pairs:
        (x, y) = pair
        reverse_pair = (y, x)
        pairs.extend([pair, reverse_pair])
    log_result = {}
    features = []
    encoded_data = []
    for trace in log:
        trace_result = {}
        for (a, b) in pairs:
            trace_result[RESPONDED_EXISTENCE + "[" + a + "," + b + "]"] = \
                mp_responded_existence(trace, True, a, b, activation_rules, correlation_rules).state.value
            trace_result[RESPONSE + "[" + a + "," + b + "]"] = \
                mp_response(trace, True, a, b, activation_rules, correlation_rules).state.value
            trace_result[ALTERNATE_RESPONSE + "[" + a + "," + b + "]"] = \
                mp_alternate_response(trace, True, a, b, activation_rules, correlation_rules).state.value
            trace_result[CHAIN_RESPONSE + "[" + a + "," + b + "]"] = \
                mp_chain_response(trace, True, a, b, activation_rules, correlation_rules).state.value
            trace_result[PRECEDENCE + "[" + a + "," + b + "]"] = \
                mp_precedence(trace, True, a, b, activation_rules, correlation_rules).state.value
            trace_result[ALTERNATE_PRECEDENCE + "[" + a + "," + b + "]"] = \
                mp_alternate_precedence(trace, True, a, b, activation_rules, correlation_rules).state.value
            trace_result[CHAIN_PRECEDENCE + "[" + a + "," + b + "]"] = \
                mp_chain_precedence(trace, True, a, b, activation_rules, correlation_rules).state.value
        if not features:
            features = list(trace_result.keys())
        encoded_data.append(list(trace_result.values()))
        log_result[trace.attributes["concept:name"]] = trace_result
    return features, encoded_data
