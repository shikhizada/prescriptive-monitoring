import numpy as np
import pandas as pd
from django.conf import settings

from sklearn import tree
from sklearn.tree import export_text
from sklearn.tree import DecisionTreeClassifier
import pydotplus
from IPython.display import Image

from declare_based.src.models import *
from declare_based.src.machine_learning.encoder import *

import os


def create_decision_tree(log, time_threshold, frequency_threshold, activation_rules, correlation_rules):
    feature_names, encoded_data = encode_trace(log=log, frequency_threshold=frequency_threshold,
                                               activation_rules=activation_rules, correlation_rules=correlation_rules)
    categories = [TraceSpeed.SLOW.value, TraceSpeed.FAST.value]

    X = pd.DataFrame(encoded_data, columns=feature_names)
    y = pd.Categorical(generate_labels(log, time_threshold), categories=categories)
    dtc = DecisionTreeClassifier(random_state=0)
    dtc.fit(X, y)

    dot_data = tree.export_graphviz(dtc, out_file=None, feature_names=feature_names, class_names=["Slow", "Fast"],
                                    filled=True, rounded=True, special_characters=True)
    graph = pydotplus.graph_from_dot_data(dot_data)
    Image(graph.create_png())
    if not os.path.exists(settings.MEDIA_ROOT + "/declare_based/output/decision_tree/"):
        os.makedirs(os.path.join(settings.MEDIA_ROOT + "/declare_based/output/decision_tree/"))
    graph.write_pdf(settings.MEDIA_ROOT + "/declare_based/output/decision_tree/decision_tree.pdf")

    tree_rules = export_text(dtc, feature_names=feature_names, show_weights=True)
    print(tree_rules)
    return dtc, feature_names


def generate_paths(tree, feature_names):
    left = tree.tree_.children_left
    right = tree.tree_.children_right
    features = [feature_names[i] for i in tree.tree_.feature]
    leaf_ids = np.argwhere(left == -1)[:, 0]
    leaf_ids_fast = filter(lambda leaf_id: tree.tree_.value[leaf_id][0][0] < tree.tree_.value[leaf_id][0][1], leaf_ids)

    def recurse(left, right, child, lineage=None):
        if lineage is None:
            lineage = []
        if child in left:
            parent = np.where(left == child)[0].item()
            state = TraceState.VIOLATED
        else:
            parent = np.where(right == child)[0].item()
            state = TraceState.SATISFIED

        lineage.append((features[parent], state))

        if parent == 0:
            lineage.reverse()
            return lineage
        else:
            return recurse(left, right, parent, lineage)

    paths = []
    for leaf_id in leaf_ids_fast:
        rules = []
        for node in recurse(left, right, leaf_id):
            rules.append(node)
        path = PathModel(impurity=tree.tree_.impurity[leaf_id], rules=rules)
        paths.append(path)
    return paths


def parse_method(method):
    method_name = method.split("[")[0]
    rest = method.split("[")[1][:-1]
    if "," in rest:
        method_params = rest.split(",")
    else:
        method_params = [rest]
    return method_name, method_params


def make_decision(partial_trace, path, activation_rules, correlation_rules):
    decision = ""
    for rule in path.rules:
        method, state = rule
        method_name, method_params = parse_method(method)
        result = DT_TRACE_METHODS[method_name](partial_trace, False, method_params[0], method_params[1],
                                               activation_rules, correlation_rules)
        print("method:", method, "state from decision tree:", state, "state from partial trace:", result.state)
        if state == TraceState.SATISFIED:
            if result.state == TraceState.VIOLATED:
                decision = "Contradiction"
                break
            elif result.state == TraceState.SATISFIED:
                pass
            elif result.state == TraceState.POSSIBLY_VIOLATED:
                decision += method + " should be SATISFIED. "
            elif result.state == TraceState.POSSIBLY_SATISFIED:
                decision += method + " should not be VIOLATED. "
        elif state == TraceState.VIOLATED:
            if result.state == TraceState.VIOLATED:
                pass
            elif result.state == TraceState.SATISFIED:
                decision = "Contradiction"
                break
            elif result.state == TraceState.POSSIBLY_VIOLATED:
                decision += method + " should not be SATISFIED. "
            elif result.state == TraceState.POSSIBLY_SATISFIED:
                decision += method + " should be VIOLATED. "
    return decision


def generate_recommendations(test_log, train_log, time_threshold, frequency_threshold, activation_rules,
                             correlation_rules):
    tree, feature_names = create_decision_tree(log=train_log, time_threshold=time_threshold,
                                               frequency_threshold=frequency_threshold,
                                               activation_rules=activation_rules,
                                               correlation_rules=correlation_rules)
    paths = generate_paths(tree, feature_names)
    recommendations = {}
    for partial_trace in test_log:
        trace_recommendations = []
        print("Case ID: ", partial_trace.attributes["concept:name"])
        for path in paths:
            recommendation = Recommendation(
                impurity=path.impurity,
                decision=make_decision(partial_trace, path, activation_rules, correlation_rules)
            )
            trace_recommendations.append(recommendation)
            print("path: ", path.rules)
            print("impurity: ", path.impurity)
            print("decision:", recommendation.decision)
            print("==================================================")
        trace_recommendations.sort(key=lambda r: r.impurity, reverse=False)
        recommendations[partial_trace.attributes["concept:name"]] = trace_recommendations
    return recommendations
