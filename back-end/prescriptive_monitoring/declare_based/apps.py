from django.apps import AppConfig


class DeclareBasedConfig(AppConfig):
    name = 'declare_based'
