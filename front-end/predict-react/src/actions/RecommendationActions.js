import {createPayloadForwardingAction} from './index';

export const RECOMMENDATION_SUBMITTED = 'RECOMMENDATION_SUBMITTED';
export const submitRecommendation = createPayloadForwardingAction(RECOMMENDATION_SUBMITTED);

export const RECOMMENDATION_SUCCEEDED = 'RECOMMENDATION_SUCCEEDED';
export const recommendationSucceeded = createPayloadForwardingAction(RECOMMENDATION_SUCCEEDED);

export const RECOMMENDATION_FAILED = 'RECOMMENDATION_FAILED';
export const recommendationFailed = createPayloadForwardingAction(RECOMMENDATION_FAILED);



export const ALARM_RESULT_SUBMITTED = 'ALARM_RESULT_SUBMITTED';
export const submitAlarmResult = createPayloadForwardingAction(ALARM_RESULT_SUBMITTED);

export const ALARM_RESULT_SUCCEEDED = 'ALARM_RESULT_SUCCEEDED';
export const alarmResultSucceeded = createPayloadForwardingAction(ALARM_RESULT_SUCCEEDED);

export const ALARM_RESULT_FAILED = 'ALARM_RESULT_FAILED';
export const alarmResultFailed = createPayloadForwardingAction(ALARM_RESULT_FAILED);


export const ALARM_METHODS_REQUESTED = 'ALARM_METHODS_REQUESTED';
export const alarmMethodsRequested = createPayloadForwardingAction(ALARM_METHODS_REQUESTED);

export const ALARM_METHODS_RETRIEVED = 'ALARM_METHODS_RETRIEVED';
export const alarmMethodsRetrieved = createPayloadForwardingAction(ALARM_METHODS_RETRIEVED);

export const ALARM_METHODS_FAILED = 'ALARM_METHODS_FAILED';
export const alarmMethodsFailed = createPayloadForwardingAction(ALARM_METHODS_FAILED);
