import {
    RECOMMENDATION_SUBMITTED,
    RECOMMENDATION_SUCCEEDED,
    RECOMMENDATION_FAILED,

    ALARM_METHODS_REQUESTED,
    ALARM_METHODS_RETRIEVED,
    ALARM_METHODS_FAILED,

    ALARM_RESULT_SUBMITTED,
    ALARM_RESULT_SUCCEEDED,
    ALARM_RESULT_FAILED
} from '../actions/RecommendationActions';

const initialState = {
    fetchState: {inFlight: false},
    recommendationList: {},
    isRecommendationListLoaded: true,
    result: {},
    isResultLoaded: true,
    methods: {},
    isMethodsLoaded: true,
    splits: [],
    isSplitsLoaded: true
};

const recommendations = (state = initialState, action) => {
        switch (action.type) {
            case RECOMMENDATION_SUBMITTED: {
                return {
                    ...state,
                    fetchState: {inFlight: true},
                    isRecommendationListLoaded: false,

                };
            }

            case RECOMMENDATION_SUCCEEDED: {
                const recommendationList = action.payload;
                return {
                    ...state,
                    fetchState: {inFlight: false},
                    recommendationList,
                    isRecommendationListLoaded: true
                };
            }

            case RECOMMENDATION_FAILED: {
                const recommendationList = initialState.recommendationList;
                return {
                    ...state,
                    fetchState: {inFlight: false, error: action.payload},
                    recommendationList,
                    isRecommendationListLoaded: true
                };
            }



            case ALARM_RESULT_SUBMITTED: {
                return {
                    ...state,
                    fetchState: {inFlight: true},
                    isResultLoaded: false,
                };
            }

            case ALARM_RESULT_SUCCEEDED: {
                const result = action.payload;
                return {
                    ...state,
                    fetchState: {inFlight: false},
                    result,
                    isResultLoaded: true
                };
            }

              case ALARM_RESULT_FAILED: {
                  const result = initialState.result;
                  return {
                      ...state,
                      fetchState: {inFlight: false, error: action.payload},
                      result,
                      isResultLoaded: true
                  };
              }



              case ALARM_METHODS_REQUESTED: {
                  return {
                      ...state,
                      fetchState: {inFlight: true},
                      isMethodsLoaded: false,
                  };
              }

              case ALARM_METHODS_RETRIEVED: {
                  const methods = action.payload;
                  return {
                      ...state,
                      fetchState: {inFlight: false},
                      methods,
                      isMethodsLoaded: true
                  };
              }

              case ALARM_METHODS_FAILED: {
                  const methods = initialState.result;
                  return {
                      ...state,
                      fetchState: {inFlight: false, error: action.payload},
                      methods,
                      isMethodsLoaded: true
                  };
              }
            default:
                return state;
        }
    }
;

export default recommendations;
