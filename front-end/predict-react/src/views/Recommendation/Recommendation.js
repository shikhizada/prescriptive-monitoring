import React, {Component} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';

import {
  SelectField, TextField, SelectionControl, Button,
  Card, CardTitle, CardText
} from 'react-md';


import {jobsRequested} from '../../actions/JobActions';
import {mapJobs, splitsToLabel} from '../../util/unNormalize';
import {logListRequested} from '../../actions/LogActions';
import {splitsRequested} from '../../actions/SplitActions';

import {submitRecommendation, alarmMethodsRequested, submitAlarmResult} from '../../actions/RecommendationActions';

// general
import Logs from '../../components/recommendation/Logs';
import Approaches from '../../components/recommendation/Approaches';
// declare-based approach
import Thresholds from '../../components/recommendation/declare/Thresholds';
import Rules from '../../components/recommendation/declare/Rules';
import Recommendations from '../../components/recommendation/declare/Recommendations';
// alarm-based approach
import Methods from '../../components/recommendation/alarm/Methods';
import Parameters from '../../components/recommendation/alarm/Parameters';
import Result from '../../components/recommendation/alarm/Result';

class Recommendation  extends Component {

  constructor(props) {
    super(props)
    this.state = {
      // declare
      thresholds: {
        "average_time": false
      },
      rules: {
        activation: "",
        correlation: ""
      },
      recommendations: {},
      // alarm
      parameters: {},
      result: {}
    }
  }

  generateClicked() {
    if (this.state.approach == "declare") {
      const payload = {
        selected_log_split_id: this.state.selectedLogSplitId,
        thresholds: this.state.thresholds,
        rules: this.state.rules
      };
      this.props.onSubmitRecommendation(payload);
    } else {
      const payload = {
        selected_log_split_id: this.state.selectedLogSplitId,
        selected_methods: this.state.selectedMethods,
        parameters: this.state.parameters
      };
      this.props.onSubmitAlarmResult(payload);
    }
  }

  componentDidMount() {
    if (this.props.jobs.length === 0) {
        this.props.onRequestLogList();
        this.props.onRequestSplitList();
        this.props.onRequestJobs();
    }
  }

  splitLabelsChanged = (id) => {
    console.log(id);
    this.setState({
      selectedLogSplitId: id
    })
  }

  approachesChanged = (approach) => {
    this.setState({
      approach: approach
    })
    if (approach == "alarm") {
      this.props.onAlarmMethodsRequested();
    }
  }

  // declare
  thresholdsChanged = (thresholds) => {
    this.setState({
      thresholds: thresholds
    })
  }

  rulesChanged = (rules) => {
    this.setState({
      rules: rules
    })
  }

  // alarm
  methodsChanged = (selectedMethods) => {
    this.setState({
      selectedMethods: selectedMethods
    })
  }

  prefixLengthChanged = (e) => {
    this.state.parameters["prefix_length"] = e
  }

  weightsChanged = (e) => {
    this.state.parameters["weights"] = e
  }

  render() {
    return (
      <div className="md-grid">
        <Logs
          splitLabelsChanged={this.splitLabelsChanged}
          splitLabels={this.props.splitLabels}
          />
          <Approaches approachesChanged={this.approachesChanged} />
          {
            this.state.approach == "declare" &&
            <Thresholds thresholdsChanged={this.thresholdsChanged} />
          }
          {
            this.state.approach == "declare" &&
            <Rules rulesChanged={this.rulesChanged} />
          }
          {
            this.state.approach == "alarm" &&
            <Methods
              methodsChanged = {this.methodsChanged}
              methods = {this.props.methods}
            />
          }
          {
            this.state.approach == "alarm" &&
            <Parameters
              prefixLengthChanged = {this.prefixLengthChanged}
              weightsChanged = {this.weightsChanged}
            />
          }
          {
            this.state.approach != null &&
            <Button className="md-cell md-cell--12" flat primary swapTheming onClick={() => this.generateClicked()}>
              Generate
            </Button>
          }
          {
            this.state.approach == "declare" &&
            <Recommendations recommendations = {this.props.recommendations} />
          }
          {
            this.state.approach == "alarm" &&
            <Result result = {this.props.result} />
          }
      </div>
    )
  }
}

Recommendation.propTypes = {
    onSubmitRecommendation: PropTypes.func.isRequired,
    recommendations: PropTypes.any,
    onAlarmMethodsRequested: PropTypes.func.isRequired,
    onSubmitAlarmResult: PropTypes.func.isRequired,
    result: PropTypes.any,
    methods: PropTypes.any
};

const mapStateToProps = (state) => ({
  jobs: mapJobs(state.logs.byId, state.splits.byId, state.jobs.byId, state.jobs.allIds),
  splitLabels: splitsToLabel(state.logs.byId, state.splits.byId, state.splits.allIds),
  logs: state.logs.byId,
  splits: state.splits.byId,

  recommendations: state.recommendations.recommendationList,
  result: state.recommendations.result,
  methods: state.recommendations.methods,
});

const mapDispatchToProps = (dispatch) => ({
    onRequestLogList: () => dispatch(logListRequested()),
    onRequestSplitList: () => dispatch(splitsRequested()),
    onRequestJobs: () => dispatch(jobsRequested()),

    onSubmitRecommendation: (payload) => dispatch(submitRecommendation({payload})),
    onAlarmMethodsRequested: (payload) => dispatch(alarmMethodsRequested({payload})),
    onSubmitAlarmResult: (payload) => dispatch(submitAlarmResult({payload})),
});

export default connect(mapStateToProps, mapDispatchToProps)(Recommendation);
