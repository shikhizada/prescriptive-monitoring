import React, {PureComponent} from 'react';
import {Card, CardTitle, CardText} from 'react-md';

class Recommendations extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      recommendations: {}
    };
  }


  componentDidUpdate() {
    this.setState({
      recommendations: this.props.recommendations["recommendations"]
    });
  }

  render() {
    return (
      <div className="md-cell md-cell--12">
        <Card className="md-block-centered">
            <CardTitle title="Result"/>
            <ul>
              {
                Object.keys(this.state.recommendations).map((key) => (
                    this.state.recommendations[key].map((value, index) => {
                      return <li key={index}>
                        <p><b>Case ID:</b> {key}</p>
                        <p><b>Impurity:</b> {value["impurity"]}</p>
                        <p><b>Decision:</b> {value["decision"]}</p>
                      </li>
                    })
                ))
              }
            </ul>
        </Card>
      </div>
    )
  }
}

export default Recommendations
