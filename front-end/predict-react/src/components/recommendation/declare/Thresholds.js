import React, {PureComponent} from 'react';
import {
  TextField, SelectionControl,
  Card, CardTitle, CardText
} from 'react-md';

class Thresholds  extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      thresholds: {}
    }
  }

  thresholdsChanged = (id, e) => {
    this.state.thresholds[id] = e
    this.props.thresholdsChanged(this.state.thresholds);
  };

  render() {
    return (
      <div className="md-cell md-cell--12">
        <Card className="md-block-centered">
            <CardTitle title="Thresholds" />
            <TextField
              id="frequency_threshold"
              label="frequency threshold"
              type="number"
              lineDirection="center"
              placeholder="between 0 and 1"
              className="md-cell"
              onChange={this.thresholdsChanged.bind(this, "frequency_threshold")}
            />
            
            <TextField
              id="time_threshold"
              label="time threshold"
              type="number"
              lineDirection="center"
              placeholder="seconds"
              className="md-cell"
              onChange={this.thresholdsChanged.bind(this, "time_threshold")}
            />

            <SelectionControl
              id="average_time"
              type="switch"
              label="Average time"
              name="Average time"
              inline
              className="md-cell"
              onChange={this.thresholdsChanged.bind(this, "average_time")}
            />
            <CardText>
              <p>If you activate <b>average time</b> then there is no need to enter <b>time threshold</b></p>
            </CardText>
          </Card>
        </div>
    )
  }
}

export default Thresholds
