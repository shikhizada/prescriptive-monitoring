import React, {PureComponent} from 'react';
import {
  TextField,
  Card, CardTitle, CardText
} from 'react-md';

class Rules  extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      rules: {}
    }
  }

  rulesChanged = (id, e) => {
    this.state.rules[id] = e
    this.props.rulesChanged(this.state.rules);
  };

  render() {
    return (
      <div className="md-cell md-cell--12">
        <Card className="md-block-centered">
            <CardTitle title="Rules"/>
            <TextField
              id="activation"
              label="activation rules"
              lineDirection="center"
              placeholder="A.attr > 6"
              className="md-cell"
              onChange={this.rulesChanged.bind(this, "activation")}
            />

            <TextField
              id="correlation"
              label="correlation rules"
              lineDirection="center"
              placeholder="T.attr < 12"
              className="md-cell"
              onChange={this.rulesChanged.bind(this, "correlation")}
            />
            <CardText>
              <p>If you do not enter the rules then it will automatically be accepted as there is no rule</p>
            </CardText>
          </Card>
        </div>
    )
  }
}

export default Rules
