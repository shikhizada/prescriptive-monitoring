import React, {PureComponent} from 'react';
import {
  Card, CardTitle, CardText,
  DataTable, TableHeader, TableBody, TableRow, TableColumn,
} from 'react-md';

class Result  extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      result: [[]]
    };
  }

  componentDidUpdate() {
    this.setState({
      result: this.props.result["result"]
    });
  }

  render() {
    return (
      <div className="md-cell md-cell--12">
        <Card className="md-block-centered">
            <CardTitle title="Result"/>
            <DataTable plain>
              <TableHeader>
                <TableRow>
                  {this.state.result[0].map(cell => (
                    <TableColumn>{cell}</TableColumn>
                  ))}
                </TableRow>
              </TableHeader>
              <TableBody>
                {this.state.result.slice(1).map((row, i) => (
                  <TableRow key={i}>
                    {row.map(cell => <TableColumn>{cell}</TableColumn>)}
                  </TableRow>
                ))}
              </TableBody>
            </DataTable>
        </Card>
      </div>
    )
  }
}

export default Result
