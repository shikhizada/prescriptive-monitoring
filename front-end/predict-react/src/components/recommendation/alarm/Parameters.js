import React, {PureComponent} from 'react';
import {
  TextField,
  Card, CardTitle, CardText,
  Grid, Cell
} from 'react-md';


class Parameters  extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      prefixLength: {},
      weights: {}
    }
  }

  prefixLengthChanged = (id, e) => {
    this.state.prefixLength[id] = Number(e)
    this.props.prefixLengthChanged(this.state.prefixLength)
  }


  weightsChanged = (id, e) => {
    this.state.weights[id] = Number(e)
    this.props.weightsChanged(this.state.weights)
  }

  render() {
    return (
      <div className="md-cell md-cell--12">
        <Card className="md-block-centered">
          <CardTitle title="Parameters"/>
            <p className="md-title md-text-center">Prefix length</p>
            <Grid>
              <Cell key={1} size={2}>
                <TextField
                  id="min"
                  label="min"
                  lineDirection="center"
                  placeholder="1 is preferable"
                  onChange={this.prefixLengthChanged.bind(this, "min")}
                />
              </Cell>
              <Cell key={1} size={2}>
                <TextField
                  id="max"
                  label="max"
                  lineDirection="center"
                  placeholder="40"
                  onChange={this.prefixLengthChanged.bind(this, "max")}
                />
              </Cell>
              <Cell key={1} size={2}>
                <TextField
                  id="quantile"
                  label="quantile"
                  lineDirection="center"
                  placeholder="between 0 and 1 (0.9 is preferable)"
                  onChange={this.prefixLengthChanged.bind(this, "quantile")}
                />
              </Cell>
            </Grid>

            <p className="md-title md-text-center">Weights</p>
            <Grid>
              <Cell key={1} size={2}>
                <TextField
                  id="c_miss_weight"
                  label="cost of undesired outcome"
                  lineDirection="center"
                  placeholder="10"
                  onChange={this.weightsChanged.bind(this, "c_miss_weight")}
                />
              </Cell>
              <Cell key={1} size={2}>
                <TextField
                  id="c_action_weight"
                  label="cost of intervention"
                  lineDirection="center"
                  placeholder="1"
                  onChange={this.weightsChanged.bind(this, "c_action_weight")}
                />
              </Cell>
              <Cell key={1} size={2}>
                <TextField
                  id="c_com_weight"
                  label="cost of compensation"
                  lineDirection="center"
                  placeholder="30"
                  onChange={this.weightsChanged.bind(this, "c_com_weight")}
                />
              </Cell>
              <Cell key={1} size={2}>
                <TextField
                  id="c_postpone_weight"
                  label="cost of postponing"
                  lineDirection="center"
                  placeholder="0 is preferable"
                  onChange={this.weightsChanged.bind(this, "c_postpone_weight")}
                />
              </Cell>
            </Grid>
          </Card>
        </div>
    )
  }
}

export default Parameters
