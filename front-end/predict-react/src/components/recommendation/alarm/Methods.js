import React, {PureComponent} from 'react';
import {
  SelectField,
  Card, CardTitle, CardText
} from 'react-md';


class Methods  extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      methods: {

      },
      selectedMethods: {}
    }
  }

  componentDidUpdate() {
    if ("methods" in this.props.methods) {
      this.setState({
        methods: this.props.methods["methods"]
      });
    }
  }

  methodsChanged = (id, e, index, value) => {
    this.state.selectedMethods[id] = e
    this.props.methodsChanged(this.state.selectedMethods);
  };

  render() {
    return (
      <div className="md-cell md-cell--12">
        <Card className="md-block-centered">
          <CardTitle title="Methods"/>
            <SelectField
              id="optimize_params_and_write_predictions"
              label="optimize params and write predictions"
              placeholder="optimize params and write predictions"
              className="md-cell"
              menuItems={this.state.methods["optimize_params_and_write_predictions"]}
              onChange={this.methodsChanged.bind(this, "optimize_params_and_write_predictions")}
            />
            <SelectField
              id="optimize_threshold_and_test"
              label="optimize threshold and test"
              placeholder="optimize threshold and test"
              className="md-cell"
              menuItems={this.state.methods["optimize_threshold_and_test"]}
              onChange={this.methodsChanged.bind(this, "optimize_threshold_and_test")}
            />
          </Card>
        </div>
    )
  }
}

export default Methods
