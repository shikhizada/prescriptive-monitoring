import React, {PureComponent} from 'react';
import {Card, CardTitle, CardText, SelectField} from 'react-md';

class Logs  extends PureComponent {

  constructor(props) {
    super(props);

    this.state = {
      splitLabels: []
    }
  }

  componentDidUpdate() {
    this.setState({
      splitLabels: this.props.splitLabels
    });
  }

  splitLabelsChanged = (e) => {
    this.props.splitLabelsChanged(e);
  };

  render() {
    return (
      <div className="md-cell md-cell--12">
        <Card className="md-block-centered">
            <CardTitle title="Logs"/>
              <SelectField
                  id="log-name-select"
                  placeholder="No log selected"
                  className="md-cell md-cell--12"
                  menuItems={this.state.splitLabels}
                  onChange={this.splitLabelsChanged}
              />
        </Card>
      </div>
    )
  }
}

export default Logs
