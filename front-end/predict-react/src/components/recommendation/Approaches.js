import React, {PureComponent} from 'react';
import {Card, CardTitle, CardText, SelectionControlGroup} from 'react-md';

class Approaches  extends PureComponent {
  constructor(props) {
    super(props);
  }

  approachesChanged = (e) => {
    this.props.approachesChanged(e);
  };

  render() {
    return (
      <div className="md-cell md-cell--12">
        <Card className="md-block-centered">
            <CardTitle title="Approaches"/>
            <SelectionControlGroup
              id="approaches"
              name="approaches"
              type="radio"
              label="Approaches"
              defaultValue=""
              controls={[{
                label: 'Declare-based approach',
                value: 'declare',
              }, {
                label: 'Alarm-based approach',
                value: 'alarm',
              }]}
              onChange={this.approachesChanged}
            />
        </Card>
      </div>
    )
  }
}

export default Approaches
